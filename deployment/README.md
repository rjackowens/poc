## Deployment Readme
Resources used to deploy kogito-springboot app and relevant infrastructure into OpenShift


#### **Usage**
- Deploys automatically via Argo CD webhook
- Run `gci *.yml | % {oc apply -f $_}` for manual deployment


#### **Features**
- Pulls latest image from Quay registry
- Deploys 1 pod with `revisionHistoryLimit` set to 5
- Requests 256 MB memory from cluster
