FROM maven:3.6.3-jdk-8 AS build-env
WORKDIR /app

# Copy pom and get dependencies as seperate layers
COPY pom.xml ./
RUN mvn dependency:resolve

# Copy everything else and build
COPY . ./
RUN mvn package -DfinalName=app

# Use runtime image
FROM openjdk:8-jre-alpine

# Set image metadata
LABEL maintainer="name@example.com"
LABEL description="Example Dockerized Spring app"

# Create new group and non-root user
RUN addgroup -S app_svc && adduser --no-create-home -S app_svc -G app_svc

# Should use port 8080 to run web server as non-root; need to find unprivileged Tomcat
EXPOSE 80

WORKDIR /app

# Reference build files from helper image
COPY --from=build-env /app/target/app.jar ./app.jar

# Run as non-root user
RUN chown -R app_svc:app_svc /app
USER app_svc

# Env var used by spring to reference the external file
ENV SPRING_CONFIG_NAME=docker-application
CMD ["/usr/bin/java", "-jar", "/app/app.jar"]
