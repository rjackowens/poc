<#
.SYNOPSIS
    Installs latest Tekton and Argo CD CRDs if not present in cluster
.EXAMPLE
    PS C:\> .\install-crds.ps1
.INPUTS
    oc_cluster_api: cluster API URL. Example: https://api.domain:6443
    oc_username: cluster username
    oc_password: cluster password
    oc_cli: full path to CLI. Example: C:\Tools\oc.exe
.OUTPUTS
    Logs to stdout and .\install_crd_log.txt
.NOTES
    Requires OpenShift CLI and permission to install CRD resources
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)] [string] $oc_cluster_api,
    [Parameter(Mandatory = $true)] [string] $oc_username,
    [Parameter(Mandatory = $true)] [securestring] $oc_password,
    [string] $oc_cli = $(
        if ($cmd = Get-Command -ErrorAction Ignore oc.exe) { # check if oc found in $env:PATH
            $cmd.Path
        }
        else {
            do {
                $user_input = Read-Host 'Specify path of executable ''oc.exe'''
            }
            until ($cmd = (Get-Command -ErrorAction Ignore $user_input))
                $cmd.Path
        }
    )
)

$crd_files = @(
    # @{name="Pipeline Operator"; project="default"; ref="openshift-pipelines-operator-rh"; yaml="?"} # see https://docs.openshift.com/container-platform/4.4/pipelines/installing-pipelines.html#op-installing-pipelines-operator-using-the-cli_installing-pipelines
    @{name="Tekton Pipelines"; project="default"; ref="pipelines.tekton.dev"; yaml="https://storage.googleapis.com/tekton-releases/pipeline/previous/v0.21.0/release.yaml​"}
    @{name="Tekton Triggers"; project="default"; ref="triggers.triggers.tekton.dev"; yaml="https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml​"}
    @{name="Tekton Dashboard"; project="default"; ref="extensions.dashboard.tekton.dev"; yaml="https://storage.googleapis.com/tekton-releases/dashboard/latest/openshift-tekton-dashboard-release.yaml"} # update namespaces
    @{name="Argo CD"; project="default"; ref="applications.argoproj.io"; yaml="https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml"}
)

function Write-Log {
    param ([string] $message)

    if (!(Test-Path ".\install_crd_log.txt" -PathType Leaf)) {New-Item ".\install_crd_log.txt"}
    "$(Get-Date -UFormat "%r")`: $message" | Tee-Object -FilePath ".\install_crd_log.txt" -Append | Write-Host
}

if (!(Test-Path $oc_cli -PathType Leaf)) {
    throw New-Object System.Exception "Unable to access OpenShift CLI"
}

Write-Log "Found OpenShift CLI"

$oc_bstr = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($oc_password))
& $oc_cli login --username $oc_username --password $oc_bstr $oc_cluster_api
if ($LASTEXITCODE -ne 0) {
    throw New-Object System.Exception "Unable to login to OpenShift cluster"
}

$crd_files | ForEach-Object {
    
    # check for CRD existence to prevent updates to existing resources
    $(& $oc_cli get crd $_.ref) | Out-Null
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Installing" $_.name"..."
        $(& $oc_cli "new-project" $_.project) | Out-Null # declarative project creation
        & $oc_cli apply -f $_.yaml -n $_.project
    }
    else {
        Write-Host $_.name "already installed"
    }
}

Write-Log "Cluster CRDs provisioned successfully"
