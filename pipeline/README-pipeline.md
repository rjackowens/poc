## Pipeline Readme
Resources used to build/test/push kogito-springboot image into Quay


#### **Usage**
- Run `.\apply-pipeline.ps1` for first time setup
- Pipeline triggers automatically on commit/creation of feature branch
- Manually trigger pipeline run via `tkn pipeline start kogito-springboot-pipeline`


#### **Features**
- Runs unit tests using `<image>` from public Quay registry
- Performs YAML validation of deployment resources
- Builds Docker image using `Buildah` from public Quay registry
- Triggers Argo CD sync on successful build
- Uses `PipelineResources` to automatically manage persistent volumes


#### **Notes**
- Requires OpenShift Pipelines Operator
- Recommend installing Tekton CLI
- Recommend installing Tekton Pipelines VS code extension
- May add `pipeline-run.yml` template