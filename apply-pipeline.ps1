<#
.SYNOPSIS
    Applies all Pipeline resources into cluster
.EXAMPLE
    PS C:\> .\apply-pipeline.ps1
.INPUTS
    oc_cluster_api: cluster API URL. Example: https://api.domain:6443
    oc_username: cluster username
    oc_password: cluster password
    oc_cli: full path to CLI. Example: C:\Tools\oc.exe
.OUTPUTS
    Logs to stdout and .\apply_pipeline_log.txt
.NOTES
    Requires OpenShift CLI and permission to apply resources
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)] [string] $oc_cluster_api,
    [Parameter(Mandatory = $true)] [string] $oc_username,
    [Parameter(Mandatory = $true)] [securestring] $oc_password,
    [string] $oc_cli = $(
        if ($cmd = Get-Command -ErrorAction Ignore oc.exe) { # check if oc found in $env:PATH
            $cmd.Path
        }
        else {
            do {
                $user_input = Read-Host 'Specify path of executable ''oc.exe'''
            }
            until ($cmd = (Get-Command -ErrorAction Ignore $user_input))
                $cmd.Path
        }
    )
)

function Write-Log {
    param ([string] $message)

    if (!(Test-Path ".\apply_pipeline_log.txt" -PathType Leaf)) {New-Item ".\apply_pipeline_log.txt"}
    "$(Get-Date -UFormat "%r")`: $message" | Tee-Object -FilePath ".\apply_pipeline_log.txt" -Append | Write-Host
}

if (!(Test-Path $oc_cli -PathType Leaf)) {
    throw New-Object System.Exception "Unable to access OpenShift CLI"
}

Write-Log "Found OpenShift CLI"

$oc_bstr = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($oc_password))
& $oc_cli login --username $oc_username --password $oc_bstr $oc_cluster_api
if ($LASTEXITCODE -ne 0) {
    throw New-Object System.Exception "Unable to login to OpenShift cluster"
}

$crd_files | ForEach-Object {
    
    # no need to perform resource check given declarative creation
    $(& $oc_cli get crd $_.ref) | Out-Null
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Installing" $_.name"..."
        $(& $oc_cli "new-project" $_.project) | Out-Null # declarative project creation
        & $oc_cli apply -f $_.yaml -n $_.project
    }
    else {
        Write-Host $_.name "already installed"
    }
}

Write-Log "Cluster CRDs provisioned successfully"
