## Kogito Springboot Cloud Native CI/CD
Demonstrates cloud native CI/CD using Tekton + Argo CD on OpenShift.

![](cicd_diagram.png)


#### **Usage**
- Run `.\install-crds.ps1`
- Run `.\apply-pipeline.ps1`
- Push code to Bitbucket repo
- Generate Bitbucket SSH access key
- Add access key to Argo CD webhook
- Create PR to trigger build and deployment


#### **Notes**
- Install Kubernetes and Tekton VS Code extensions for linting support
- App Name: kogito-springboot
